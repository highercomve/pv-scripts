#!/bin/bash

set -ex

export PANTAVISOR_VERSION="${PANTAVISOR_VERSION:-NA}"
export PANTAVISOR_DEBUG=${PANTAVISOR_DEBUG:-no}

TARGET=$1
export TARGET

TOP_DIR=$(cd $(dirname $0) && pwd -P)

cpus=`cat /proc/cpuinfo | grep processor | wc -l`
threads=$(($cpus + 1))

if test -z "$MAKEFLAGS"; then
	MAKEFLAGS="-j$threads"
fi

export MAKEFLAGS

target=
uboot=

udev_update() {
	if test -n "${IMG_DEVICE}"; then
		udevadm trigger
		udevadm settle
	fi
}

setup_kernel_atom() {
	find "$TOP_DIR/kernel/" -not -path "$TOP_DIR/kernel/modules/*" -iname "atom.mk" | xargs rm -f
	kerneldir=$TOP_DIR/kernel/$kernel.$subtarget
	if ! [ -d $kerneldir ]; then
		kerneldir=$TOP_DIR/kernel/$kernel
	fi
	if [ ! -f "$kerneldir/atom.mk" ]; then
		echo "Setting up $kerneldir kernel"
		ln -s "$TOP_DIR/scripts/atoms/kernel-mk" "$kerneldir/atom.mk"
	fi
}

setup_uboot() {
	find "$TOP_DIR/bootloader/" -iname "atom.mk" | xargs rm -f
	ubootdir=$TOP_DIR/bootloader/$uboot.$subtarget
	if ! [ -d $ubootdir ]; then
		ubootdir=$TOP_DIR/bootloader/$uboot
	fi
	if [ ! -z "$uboot" ]; then
		if [ ! -f "$ubootdir/atom.mk" ]; then
			echo "Setting up $ubootdir bootloader"
			ln -s "$TOP_DIR/scripts/atoms/uboot-mk" "$ubootdir/atom.mk"
		fi
	fi
}

setup_alchemy () {
	export ALCHEMY_HOME=${TOP_DIR}/alchemy
	export ALCHEMY_WORKSPACE_DIR=${TOP_DIR}

	export ALCHEMY_TARGET_PRODUCT=traild
	export ALCHEMY_TARGET_PRODUCT_VARIANT=$target
	export ALCHEMY_TARGET_CONFIG_DIR=${TOP_DIR}/config/${ALCHEMY_TARGET_PRODUCT_VARIANT}
	export ALCHEMY_TARGET_OUT=${TOP_DIR}/out/${ALCHEMY_TARGET_PRODUCT_VARIANT}
	if [ -n "$subtarget" ]; then
		ALCHEMY_TARGET_OUT=$ALCHEMY_TARGET_OUT-$subtarget
	fi
	export ALCHEMY_USE_COLORS=1

	export TRAIL_BASE_DIR=${ALCHEMY_TARGET_OUT}/trail/
	export TARGET_VENDOR_DIR=${TOP_DIR}/vendor/${ALCHEMY_TARGET_PRODUCT_VARIANT}
	export PVR="${TOP_DIR}/scripts/pvr/pvr"
	if [ "${PANTAVISOR_DEBUG}" == no ]; then
		export KCPPFLAGS='-DENV_DEVICE_SETTINGS_EXTRA=\"silent=1\\0\"'
	fi
	export TEZI_OUT_DIR=${ALCHEMY_TARGET_OUT}/tezi
}

build_mmc_tools() {
	${ALCHEMY_HOME}/scripts/alchemake host.e2fsprogs
	${ALCHEMY_HOME}/scripts/alchemake host.mtools
}

build_mmc_image() {
	export DEBUGFS=${ALCHEMY_TARGET_OUT}/staging-host/usr/sbin/debugfs
	POPULATEEXTFS=${ALCHEMY_TARGET_OUT}/staging-host/bin/populate-extfs.sh
	MCOPY=${ALCHEMY_TARGET_OUT}/staging-host/usr/bin/mcopy
	if test ! -e $MCOPY; then
		echo cannot find mcopy from mtools to generate vfat partition
		exit 36
	fi
	if test ! -e $POPULATEEXTFS; then
		echo cannot find populate-extfs.sh script to build trail storage
		exit 37
	fi
	PI=$target
	if [ -e config/${target}/image.config ]; then
		source config/${target}/image.config
	fi

	echo making base mmc image
	tmpimg=${IMG_DEVICE:-`mktemp`}
	if [ "$IMAGE_TYPE" == "tezi" ]; then
		rm -f $tmpimg
		tmpimg=
		mkdir -p $TEZI_OUT_DIR
	elif [ -z "$IMG_DEVICE" -o -n "$IMG_DEVICE_CLEAN" ] && [ -n "$MMC_SIZE" ]; then
		dd if=/dev/zero of=$tmpimg bs=1M count=0 seek=$MMC_SIZE
		sync $tmpimg
	elif [ -n "$IMG_DEVICE" ]; then
		dd if=/dev/zero of=$tmpimg bs=1M count=1
		sync $tmpimg
	else
		tmpimg=
	fi
	if test $? -ne 0; then
		echo error creating disk image
		exit 5
	fi
	echo making boot part
	if [ -n "$tmpimg" ] && [ "$PI" == "bbb" ]; then
		dd if=${ALCHEMY_TARGET_OUT}/build/uboot/MLO of=$tmpimg bs=512 seek=256 count=256 conv=notrunc
		dd if=${ALCHEMY_TARGET_OUT}/build/uboot/u-boot.img of=$tmpimg bs=512 seek=768 count=1024 conv=notrunc
		cp ${ALCHEMY_TARGET_OUT}/build/linux/arch/arm/boot/dts/am335x-boneblack.dtb ${ALCHEMY_TARGET_OUT}/trail/final/trails/0/.pv
	fi

	if [ "$IMAGE_TYPE" == "tezi" ]; then
		echo "building tezi artifacts, seeding skeleton structure to $TEZI_OUT_DIR ..."
		cp -rvf ${TOP_DIR}/vendor/${target}/tezi/* $TEZI_OUT_DIR/
	elif [ -z "$tmpimg" ]; then
		echo "building root tarball, not image. skipping preparing the boot partition ..."
	elif [ "$PI" == "rpi3" ] || [ "$PI" == "rpi4" ] || [ "$PI" == "rpi0w" ] || [ "$PI" == "bbb" ]; then
		parted -s $tmpimg -- mklabel msdos \
			mkpart p fat32 1MiB $(($BOOT_SIZE + 1))MiB
	elif [ "$PI" == "bpi-r2" ]; then
		parted -s $tmpimg -- mklabel msdos \
			mkpart p fat32 100MiB $(($BOOT_SIZE + 100))MiB
		cp -f $tmpimg ${TOP_DIR}/out/backup1
	else
		parted -s $tmpimg -- mklabel gpt \
			mkpart ESP fat32 1MiB $(($BOOT_SIZE + 1))MiB \
			set 1 boot on
	fi

	if test $? -ne 0; then
		echo error partitioning image file
		exit 6
	fi
	sync $tmpimg
	udev_update

	boot_scr=${TOP_DIR}/config/${target}/boot.scr
	uboot_env=${TOP_DIR}/config/${target}/uboot.env
	if [ "${PANTAVISOR_DEBUG}" == "no" ]; then
		if [ -e ${TOP_DIR}/config/${target}/boot${subtarget:+.${subtarget}}.scr ]; then
			boot_scr=${TOP_DIR}/config/${target}/boot${subtarget:+.${subtarget}}.scr
		fi
		if [ -e ${TOP_DIR}/config/${target}/uboot${subtarget:+.${subtarget}}.env ]; then
			uboot_env=${TOP_DIR}/config/${target}/uboot${subtarget:+.${subtarget}}.env
		fi
	else
		if [ -e ${TOP_DIR}/config/${target}/boot${subtarget:+.${subtarget}}.debug.scr ]; then
			boot_scr=${TOP_DIR}/config/${target}/boot${subtarget:+.${subtarget}}.debug.scr
		elif [ -e ${TOP_DIR}/config/${target}/boot.debug.scr ]; then
			boot_scr=${TOP_DIR}/config/${target}/boot.debug.scr
		elif [ -e ${TOP_DIR}/config/${target}/boot${subtarget:+.${subtarget}}.scr ]; then
			boot_scr=${TOP_DIR}/config/${target}/boot${subtarget:+.${subtarget}}.scr
		fi

		if [ -e ${TOP_DIR}/config/${target}/uboot${subtarget:+.${subtarget}}.debug.env ]; then
			uboot_env=${TOP_DIR}/config/${target}/uboot${subtarget:+.${subtarget}}.debug.env
		elif [ -e ${TOP_DIR}/config/${target}/uboot.debug.env ]; then
			uboot_env=${TOP_DIR}/config/${target}/uboot.debug.env
		elif [ -e ${TOP_DIR}/config/${target}/uboot${subtarget:+.${subtarget}}.env ]; then
			uboot_env=${TOP_DIR}/config/${target}/uboot${subtarget:+.${subtarget}}.env
		fi
	fi

	tmpfs=
	tmpfs=`mktemp`
	if [ "$IMAGE_TYPE" == tezi ]; then

		bootbasename=`cat $TEZI_OUT_DIR/image.$subtarget.json \
			| jq -r '.blockdevs[] | select (.partitions) | .partitions[] | select (.content.label == "pvboot") | .content.filename' 2>/dev/null || true`

		if [ -z "$bootbasename" ]; then
			bootbasename=`cat $TEZI_OUT_DIR/image.$subtarget.json \
				| jq -r '.mtddevs[] | select (.ubivolumes) | .ubivolumes[] | select (.name == "pvboot") | .content.filename'`
		fi

		if [ -z "$bootbasename" ]; then
			echo "No boot base name found in $TEZI_OUT_SDIR/image.$subtarget.json"
			exit 4
		fi

		cp -f $boot_scr $TEZI_OUT_DIR/boot.scr

		# for tezi we just need the nice boot.scr as defined on target for bootfs
		tar -C $TEZI_OUT_DIR/ -cvJf $TEZI_OUT_DIR/$bootbasename boot.scr

		rootbasename=`cat $TEZI_OUT_DIR/image.$subtarget.json \
			| jq -r '.blockdevs[] | select (.partitions) | .partitions[] | select (.content.label == "pvroot") | .content.filename' 2>/dev/null || true`

		if [ -z "$rootbasename" ]; then
			rootbasename=`cat $TEZI_OUT_DIR/image.$subtarget.json \
				| jq -r '.mtddevs[] | select (.ubivolumes) | .ubivolumes[] | select (.name == "pvroot") | .content.filename'`
		fi
		if [ -z "$rootbasename" ]; then
			echo "No root base name found in $TEZI_OUT_SDIR/image.$subtarget.json"
			exit 5
		fi

		# for tezi we just need the nice boot.scr as defined on target for bootfs
		tar -C ${ALCHEMY_TARGET_OUT}/trail/final/ -cvJf $TEZI_OUT_DIR/$rootbasename .

		cat $TEZI_OUT_DIR/image.$subtarget.json | jq ". | .release_date=\"`date +%Y-%m-%d`\"" > $TEZI_OUT_DIR/image.json 
		rm -f $TEZI_OUT_DIR/image.$subtarget.json

		echo "{}" | jq "{ config_format: 1,  images: [\"image.json\"]} " > $TEZI_OUT_DIR/image_list.json
		echo "{}" | jq "{ config_format: 1, image_lists: [\"image_list.json\"], show_default_feed: false, show_3rdparty_feed: false}" > $TEZI_OUT_DIR/tezi_config.json

		tar -cvzf $ALCHEMY_TARGET_OUT/${TARGET}-pv-tezi.tar.gz -C $TEZI_OUT_DIR/ .

		echo "TEZI tarball is here: $ALCHEMY_TARGET_OUT/${TARGET}-pv-tezi.tar.gz"
		echo "Have fun!"

		# for now we exit early. XXX: review what features from below would be good for tezi also
		exit 0

	elif [ -n "$BOOT_SIZE" ]; then

		echo making vfat boot fs image with size ${BOOT_SIZE}MiB
		dd if=/dev/zero of=$tmpfs bs=1M count=0 seek=$BOOT_SIZE
		mkfs.vfat -n pvboot $tmpfs
		echo copying boot contents to \"$tmpfs\"
		if [ -d "${TOP_DIR}/vendor/${target}/boot/" ]; then
			$MCOPY -v -i $tmpfs -s ${TOP_DIR}/vendor/${target}/boot/* ::/
		fi
		if [ -d "${ALCHEMY_TARGET_OUT}/final/boot/" ]; then
			$MCOPY -i $tmpfs -s ${ALCHEMY_TARGET_OUT}/final/boot/u-boot.bin ::/uboot.bin
			if [ -e $boot_scr ]; then
				$MCOPY -i $tmpfs -s $boot_scr ::/boot.scr
			elif [ -e $uboot_env ]; then
				$MCOPY -i $tmpfs -s $uboot_env ::/uboot.env
			fi
		fi
		bootfs=$tmpfs
		sync $bootfs
	fi

	# if we have a boot-installer directory in vendor/ we will overlay it and use that for the actual
	# installer image ...
	if (echo $TARGET | grep -q -- -installer) && [ -d ${TOP_DIR}/vendor/${target}/boot-installer ]; then
		echo "making installer fat partition 'pvfboot'"
		tmpfs=`mktemp`
		cp $bootfs $tmpfs
		fatlabel $tmpfs pvfboot
		$MCOPY -D o -v -i $tmpfs -s ${TOP_DIR}/vendor/${target}/boot-installer/grub ::/
		$MCOPY -D o -v -i $tmpfs -s ${TOP_DIR}/vendor/${target}/boot-installer/EFI ::/
		sync $tmpfs
	fi

	echo writing boot fs to disk image part 1
	if [ -n "$tmpimg" ]; then
		if [ "$PI" == "bpi-r2" ]; then
			# bpi-r2 has a 100MB no-partition offset for bootloader and stuff
			dd conv=notrunc if=$tmpfs of=$tmpimg bs=1M seek=100
		else
			dd conv=notrunc if=$tmpfs of=$tmpimg bs=1M seek=1
		fi
		sync $tmpimg
		echo boot fs written to disk image part 1

		if [ "$PI" == "bpi-r2" ]; then
			size_i=$(($BOOT_SIZE + 100))
		else
			size_i=$(($BOOT_SIZE + 1))
		fi

		part_i=2
		for part_size in ${MMC_OTHER_PART_SIZES}; do
			echo making ext4 data fs image with size ${part_size}MiB
			tmpfs=`mktemp`
			dd if=/dev/zero of=$tmpfs bs=1M count=0 seek=$part_size
			fakeroot mkfs.ext4 -L pvol$part_i $tmpfs
			sync $tmpfs
			zerofree $tmpfs
			seek=$(($size_i * 1024))
			echo writing other part fs to disk image part $part_i with seek=${seek}KiB
			parted -s $tmpimg -- \
				${MMC_OTHER_MKPART}
			dd conv=notrunc if=$tmpfs of=$tmpimg bs=1K seek=$seek
			sync $tmpimg
			size_i=$(($size_i + $part_size))
			part_i=$(($part_i + 1))
			rm -f $tmpfs
		done
	fi

	# OEM config
	_config_bin_dir=${TOP_DIR}/out/${target}/binary_config
	_config_bin_file=${_config_bin_dir}/compaq.bin
	mkdir -p $_config_bin_dir
	rm -f $_config_bin_file

	_root_label=pvroot
	_root_src_dir=${ALCHEMY_TARGET_OUT}/trail/final/
	_delete_dirs=
	if echo $TARGET | grep -q -- -installer; then
		_root_label=pvdata
		_orig_src_dir=$_root_src_dir
		_root_src_dir=`mktemp -d -t pvinstall-data.XXXXXX`
		_delete_dirs="$delete_dirs $_root_src_dir"

		mkdir -p $_root_src_dir/.pvf/

		# install files in vendor pvfactory dir
		cp -rf ${TOP_DIR}/vendor/${target}/pvfactory/* $_root_src_dir/.pvf

		# local pvfactory env; use for vendor settings/credentials etc.
		if [ -n "$PV_FACTORY_LOCAL" -a -d "$PV_FACTORY_LOCAL" ]; then
			cp -rfv ${PV_FACTORY_LOCAL}/* $_orig_src_dir/.pvf/
		fi

		# XXX TODO
		tmpf=`mktemp`
		sha256sum -b ${TOP_DIR}/vendor/${target}/boot/grub/grub.cfg | awk '{ print $1 }' > $tmpf

		cp -f $tmpf $_root_src_dir/.pvf/config/grub.cfg.sha256
		cp -f ${TOP_DIR}/out/${target}/trail/final/trails/0/.pv/pv-kernel.img $_root_src_dir/.pvf/pv-kernel.img

		if [ "$SDO_INSTALLER" = "yes" ]; then
			cp -f ${TOP_DIR}/vendor/${target}/boot-installer/pv-installer-sdo.img $_root_src_dir/.pvf/pv-installer.img
		else
			cp -f ${TOP_DIR}/vendor/${target}/boot-installer/pv-installer.img $_root_src_dir/.pvf/pv-installer.img
		fi

		# pack up root
		fakeroot tar -C $_orig_src_dir -cvJf $_root_src_dir/.pvf/root.tar.xz .

		if [ -n "$PV_AUTOTOK_FILE" ]; then
			mkdir -p $_root_src_dir/config/
			if [ ! -f "$PV_AUTOTOK_FILE" ]; then
				PANTAHUB_HOST=${PANTAHUB_HOST:-api.pantahub.com}
				PANTAHUB_PORT=${PANTAHUB_PORT:-443}
				PH_BASE_URL="https://$PANTAHUB_HOST:$PANTAHUB_PORT"
				echo "Lets create a new device token to be used by the pvf-installer"
				echo "Enter your pantahub username:"
				read username
				echo "Enter your password: (password won't be show)"
				read -s password
				
				# Get user token to be used to create the device_token
				response=$(curl --request POST ${PH_BASE_URL}/auth/login \
					--header 'content-type: application/json' \
					--data '{"username": "'${username}'","password": "'${password}'"}')

				token=`echo $response | jq -r '.token'`
				error=`echo $response | jq -r '.Error'`

				if [ "$token" == "null" ] || [ "$error" != "null" ]; then
					echo "server error: $error"
					echo ""
					echo "Your aren't authorized, review the username and password you used here."
					echo ""
					
					exit 1
				fi

				# Create a new device token
				response=`curl --request POST ${PH_BASE_URL}/devices/tokens --header "Authorization: Bearer $token"`
				
				id=`echo $response | jq -r '.id'`
				error=`echo $response | jq -r '.Error'`
				if [ "$id" == "null" ] || [ "$error" != "null" ]; then
					echo "server error: $error"
					echo ""
					echo "We can't get a device token correctly, try to get it and put it in $PV_AUTOTOK_FILE"
					echo "In order to get the device token you could use"
					echo "curl --request POST ${PH_BASE_URL}/devices/tokens --header 'Authorization: Bearer $token'"
					echo ""
					exit 1
				fi
				echo $response > $PV_AUTOTOK_FILE
			fi

			echo -n -e "autojointoken.aca.json=$(cat $PV_AUTOTOK_FILE)\0" >> $_config_bin_file
		fi

		# pack up boot
		cp -f $bootfs $_root_src_dir/.pvf/boot.bin.$BOOT_SIZE
	fi
	rm -f $bootfs

	# inject auto tok into OEM configuration partition (XXX: fix for grub)
	if [ -n "$PV_FACTORY_AUTOTOK" ]; then
		echo -n -e "configargs=ph_factory.autotok=${PV_FACTORY_AUTOTOK}\0" >> $_config_bin_file
	fi

	# add 1M control partition for OEM configuration
	if [ -n "$tmpimg" ]; then
		echo -n -e "\0" >> $_config_bin_file
		echo "creating control partition"
		parted -s $tmpimg -- \
			mkpart p fat32 ${size_i}MiB $(($size_i + 1))MiB
		( echo t; echo 2; echo 17; echo w; echo q ) | fdisk $tmpimg
		offset=`sfdisk -J $tmpimg | jq -r .partitiontable.partitions[1].start`
		size=`sfdisk -J $tmpimg | jq -r .partitiontable.partitions[1].size`
		if [ -f $_config_bin_file ]; then
			dd conv=notrunc if=$_config_bin_file of=$tmpimg bs=512 seek=$offset count=$size
		else
			dd conv=notrunc if=/dev/zero of=$tmpimg bs=512 seek=$offset count=$size
		fi
		size_i=$(($size_i + 1))
		sync $tmpimg
	fi

	if [ "$IMAGE_TYPE" == tezi ] || [ -z "$tmpimg" ]; then
		echo "making root as tarball."
		fakeroot tar -C $_root_src_dir -cvJf ${ALCHEMY_TARGET_OUT}/${TARGET}-rootfs.tar.xz .
	elif test -z "$IMG_DEVICE"; then
		DATA_SIZE=$(($MMC_SIZE - $size_i - 1))
		SEEK_K=$(($size_i*1024))
		echo making ext4 data fs image for pv storage ${DATA_SIZE}MiB
		tmpfs=`mktemp`
		dd if=/dev/zero of=$tmpfs bs=1M count=0 seek=$DATA_SIZE
		fakeroot mkfs.ext4 -L $_root_label $tmpfs
		sync $tmpfs
		echo copying trail storage data to \"$tmpfs\"
		fakeroot $POPULATEEXTFS $_root_src_dir $tmpfs
		sync $tmpfs
		zerofree $tmpfs
		echo writing trail data fs to disk image part 2 with seek in KiB=$SEEK_K
		parted -s $tmpimg -- \
			mkpart p ext4 ${size_i}MiB -1MiB
		dd conv=notrunc if=$tmpfs of=$tmpimg bs=1K seek=${SEEK_K}
		sync $tmpimg
		echo trail data fs written to disk image part 2
		rm -f $tmpfs
	else
		echo making ext4 data fs on device ${IMG_DEVICE}${part_i}
		parted -s ${IMG_DEVICE} -- \
			mkpart p ext4 ${size_i}MiB 100%
		udev_update
		fakeroot mkfs.ext4 -L pvroot ${IMG_DEVICE}${part_i}
		sync
		mntp=`mktemp -d`
		mount ${IMG_DEVICE}${part_i} $mntp
		tar -C $_root_src_dir -c . | tar -C $mntp -xv
		umount $mntp
		rmdir $mntp
		sync $IMG_DEVICE
	fi

	if [ -n "$_delete_dirs" ]; then
		for d in $_delete_dirs; do
			rm -rf $d
		done
	fi

	if test "$IMAGE_TYPE" == tezi; then
		echo "TEZI image type"

	elif test -z "$IMG_DEVICE" && test -n "$tmpimg" ; then
		if [ "$PI" == "bpi-r2" ]; then
			dd if=${TOP_DIR}/vendor/${target}/SD/BPI-R2-HEAD440-0k.img of=$tmpimg conv=notrunc
			dd if=${TOP_DIR}/vendor/${target}/SD/BPI-R2-HEAD1-512b.img of=$tmpimg bs=512 seek=1 conv=notrunc
			gunzip -c ${TOP_DIR}/vendor/${target}/SD/BPI-R2-720P-2k.img.gz | dd of=$tmpimg bs=1024 seek=2 conv=notrunc
			if [ -f ${ALCHEMY_TARGET_OUT}/final/boot/u-boot.bin ]; then
				 cat ${ALCHEMY_TARGET_OUT}/final/boot/u-boot.bin | dd of=$tmpimg bs=1024 seek=320 conv=notrunc
			elif [ -f ${TOP_DIR}/vendor/${target}/SD/BPI-R2_u-boot.bin ]; then
				 cat ${TOP_DIR}/vendor/${target}/SD/BPI-R2_u-boot.bin | dd of=$tmpimg bs=1024 seek=320 conv=notrunc
			fi
		fi

		mv $tmpimg $ALCHEMY_TARGET_OUT/${TARGET}-pv-${MMC_SIZE}MiB.img
		echo -e "\nmmc image avaialble at ${ALCHEMY_TARGET_OUT}/${TARGET}-pv-${MMC_SIZE}MiB.img"
		echo please flash onto ${PI} sd card with dd

	elif test -n "$IMG_DEVICE"; then
		if [ "$PI" == "bpi-r2" ]; then
			dd if=${TOP_DIR}/vendor/${target}/SD/BPI-R2-HEAD440-0k.img of=$IMG_DEVICE conv=notrunc
			dd if=${TOP_DIR}/vendor/${target}/SD/BPI-R2-HEAD1-512b.img of=$IMG_DEVICE bs=512 seek=1 conv=notrunc
			gunzip -c ${TOP_DIR}/vendor/${target}/SD/BPI-R2-720P-2k.img.gz | dd of=$IMG_DEVICE bs=1024 seek=2 conv=notrunc
			if [ -f ${ALCHEMY_TARGET_OUT}/final/boot/u-boot.bin ]; then
				 cat ${ALCHEMY_TARGET_OUT}/final/boot/u-boot.bin | dd of=$tmpimg bs=1024 seek=320 conv=notrunc
			elif [ -f ${TOP_DIR}/vendor/${target}/SD/BPI-R2_u-boot.bin ]; then
				 cat ${TOP_DIR}/vendor/${target}/SD/BPI-R2_u-boot.bin | dd of=$tmpimg bs=1024 seek=320 conv=notrunc
			fi
		fi
		echo -e "\nmmc image flashed on block device ${IMG_DEVICE}"
	fi

}

build_target_bsp() {
	echo BUILDING target: $target
	echo BUILDING uboot: $uboot
	if [ "$PVR_USE_SRC_BSP" != "yes" ]; then
		${ALCHEMY_HOME}/scripts/alchemake all
		${ALCHEMY_HOME}/scripts/alchemake final
		${ALCHEMY_HOME}/scripts/alchemake image
	elif [ -n "$uboot" ]; then
		${ALCHEMY_HOME}/scripts/alchemake uboot
		${ALCHEMY_HOME}/scripts/alchemake final
	fi
}

case $TARGET in
arm-bpi-r2)
	export LOADADDR=0x80008000
	target="bpi-r2"
	kernel="linux-bpi-r2-owrt"
	uboot="bpi-r2"
	;;
arm-qemu)
	target="vexpress-a9"
	kernel="vexpress-a9"
	uboot="vexpress-a9"
	;;
malta-qemu)
	target="malta"
	kernel="malta"
	uboot="malta"
	;;
legacy-qemu)
	export BL_IS_PVK="yes"
	target="legacy"
	kernel="malta"
	uboot="malta"
	;;
mips-mt300a)
	export PV_NO_UBOOT=1
	export PV_BL_IS_PVK="yes"
	target="mt300a"
	kernel="mt300a"
	;;
mipsel)
	target="mipsel"
	;;
mips-generic)
	target="mips-generic"
	kernel=""
	uboot=""
	;;
arm-generic)
	target="arm-generic"
	kernel=""
	uboot=""
	;;
x64-generic)
	target="x64-generic"
	kernel=""
	uboot=""
	;;
arm-rpi0w-mmc)
	target="rpi0w"
	setup_alchemy
	build_mmc_image
	exit 0
	;;
arm-rpi0w)
	export LOADADDR=0x00008000
	target="rpi0w"
	kernel="rpi-4.14.y"
	uboot="rpi3"
	;;
arm-rpi2-mmc)
	target="rpi2"
	setup_alchemy
	build_mmc_image
	exit 0
	;;
arm-rpi2)
	target="rpi2"
	kernel="rpi3"
	uboot="rpi3"
	;;
arm-rpi3-mmc)
	target="rpi3"
	setup_alchemy
	build_mmc_image
	exit 0
	;;
arm-rpi3)
	export LOADADDR=0x00008000
	target="rpi3"
	kernel="rpi3"
	uboot="rpi3"
	;;
arm-rpi4-mmc)
	target="rpi4"
	setup_alchemy
	build_mmc_image
	exit 0
	;;
arm-rpi4)
	export LOADADDR=0x00008000
	target="rpi4"
	kernel="rpi3"
	uboot="rpi4"
	;;
arm-bbb)
	export LOADADDR=0x80008000
	target="bbb"
	kernel="bbb"
	uboot="bbb"
	;;
arm-toradex-*)
	export LOADADDR=0x11000000
	target=toradex
	subtarget=${TARGET#arm-toradex-}
	kernel=linux-stable
	uboot=u-boot-toradex
	;;
arm-mvebu)
	export LOADADDR=0x00008000
	target="mvebu"
	kernel="linux-openwrt-xp"
	uboot=""
	;;
arm64-hikey)
	target="hikey"
	kernel="hikey"
	;;
x64-uefi)
	target="x64-uefi"
	kernel="linux-stable"
	;;
x64-uefi-installer)
	target="x64-uefi"
	kernel="linux-stable"
	;;
*)
	echo "Must define target product as first argument [arm-qemu, malta-qemu, arm-generic, mips-generic, x64-generic, arm-mvebu, arm-rpi3, arm-rpi2, arm-rpi4, x64-uefi]"
	exit 1
	;;
esac

if [ "$PV_INSTALLER" = "yes" ] && [ "$target" = "x64-uefi" ]; then
	TARGET="x64-uefi-installer"
elif [ "$PV_INSTALLER" = "yes" ]; then
	echo "PV_INSTALLER only supported by x64-uefi target"
	exit 1
fi

# if we have a subtarget we use a subtarget config
SUBTARGET=$subtarget
export SUBTARGET

setup_alchemy

if test -z "$PANTAHUB_HOST"; then
	PANTAHUB_HOST=api.pantahub.com
fi
if test -z "$PANTAHUB_PORT"; then
	PANTAHUB_PORT=443
fi


if [ ! -z "$2" ]; then
	if [ "$2" == "upload" ]; then
		cd $TRAIL_BASE_DIR/staging
		$PVR putobjects -f https://$PANTAHUB_HOST:$PANTAHUB_PORT/objects
		cd $TOP_DIR
	else
		${ALCHEMY_HOME}/scripts/alchemake "${@:2}"
	fi
elif [ "$target" == "malta" -o "$target" == "vexpress-a9" -o "$target" == "legacy" -o "$target" == "mt300a" ]; then

	echo "Building $target target"
	setup_kernel_atom
	setup_uboot
	if [ ! -f ${ALCHEMY_TARGET_OUT}/build-host/qemu/qemu.done ]; then
		${ALCHEMY_HOME}/scripts/alchemake host.qemu
	fi

	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail
	${ALCHEMY_HOME}/scripts/alchemake ubitrail
	${ALCHEMY_HOME}/scripts/alchemake pflash

elif [ "$target" == "mipsel" ]; then

	echo "Building $target target"
	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail

elif [ "$target" == "arm-generic" ]; then

	echo "Building $target target"
	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image

elif [ "$target" == "mips-generic" ]; then

	echo "Building $target target"
	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail

elif [ "$target" == "x64-generic" ]; then

	echo "Building $target target"
	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail

elif [ "$target" == "rpi0w" ]; then

	echo "Building $target target"
	setup_kernel_atom
	setup_uboot
	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
elif [ "$target" == "rpi2" ]; then

	echo "Building $target target"
	setup_kernel_atom
	setup_uboot
	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
elif [ "$target" == "rpi3" -o "$target" == "rpi4" ]; then

	echo "Building $target target"
	setup_kernel_atom
	setup_uboot
	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
elif [ "$target" == "bpi-r2" ]; then

	echo "Building $target target"
	setup_kernel_atom
	setup_uboot
	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
elif [ "$target" == "bbb" ]; then

	echo "Building $target target"
	setup_kernel_atom
	setup_uboot
	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
elif [ "$target" == "mvebu" ]; then

	echo "Building $target target"
	setup_kernel_atom
	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail
	${ALCHEMY_HOME}/scripts/alchemake ubitrail

elif [ "$target" == "toradex" ]; then
	setup_kernel_atom
	setup_uboot
	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
elif [ "$target" == "hikey" ]; then

	echo "Building $target target"
	setup_kernel_atom
	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
elif [ "$target" == "x64-uefi" ]; then

	echo "Building $target target"
	setup_kernel_atom
	build_target_bsp

	${ALCHEMY_HOME}/scripts/alchemake trail

	build_mmc_tools
	build_mmc_image
fi
